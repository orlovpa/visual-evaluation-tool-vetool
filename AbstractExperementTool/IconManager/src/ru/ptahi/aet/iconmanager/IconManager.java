package ru.ptahi.aet.iconmanager;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import sun.awt.image.ToolkitImage;

/**
 *
 * @author paulorlov
 */
public class IconManager {

    private Map<String, Image> map = new HashMap();
    private Map<String, javafx.scene.image.Image> mapFX = new HashMap();
    private static IconManager iconManagerObj;
    
    private IconManager(){

    }
    
    private Image scaleImage(Image iObj) {
        int bType = BufferedImage.TYPE_INT_ARGB;
        BufferedImage iconImage = new BufferedImage(16, 16, bType);
        Graphics2D g = iconImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(iObj, 0, 0, 16, 16, null);
        g.dispose();
        return iconImage;
    }    
    
    private Image scaleImage(Image iObj, int w, int h) {
        int bType = BufferedImage.TYPE_INT_ARGB;
        BufferedImage iconImage = new BufferedImage(w, h, bType);
        Graphics2D g = iconImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(iObj, 0, 0, w, h, null);
        g.dispose();
        return iconImage;
    }

    private void registerImage(String iconName) {
        Image iObj = ImageUtilities.loadImage(iconName);
        if(iObj.getHeight(null) != 16){
            iObj = scaleImage(iObj);
        }
        map.put(iconName, iObj);
    }

    public Map<String, Image> getMap() {
        return map;
    }
    
    public Map<String, javafx.scene.image.Image> getFXMap() {
        return mapFX;
    }
    
    public static IconManager getIconManager(){
        if(iconManagerObj == null){
            iconManagerObj = new IconManager();
        }
        return iconManagerObj;
    }
    
    public static Image getActiveIcon(String iconName){
        IconManager currentIMObj = getIconManager();
        Map mObj = currentIMObj.getMap();
        String key = "icons/black/" + iconName;
        if(!mObj.containsKey(key)){
            currentIMObj.registerImage(key);
        }
        return (Image) mObj.get(key);
    }
    
    public static Image getOpenedIcon(String iconName){
        IconManager currentIMObj = getIconManager();
        Map mObj = currentIMObj.getMap();
        String key = "icons/blue/" + iconName;
        if(!mObj.containsKey(key)){
            currentIMObj.registerImage(key);
        }
        return (Image) mObj.get(key);
    }
    
    public static javafx.scene.image.Image getFXIcon(String iconName){
        IconManager currentIMObj = getIconManager();
        Map mObj = currentIMObj.getFXMap();
        String key = "icons/black/" + iconName;
        if(!mObj.containsKey(key)){
            currentIMObj.registerFXImage(key);
        }
        return (javafx.scene.image.Image) mObj.get(key);
    }

    private void registerFXImage(String iconName) {
        Image iObj = ImageUtilities.loadImage(iconName);
        if(iObj.getHeight(null) != 16){
            iObj = scaleImage(iObj);
        }
        
        BufferedImageBuilder builder = new BufferedImageBuilder();
        BufferedImage bImage = builder.bufferImage(iObj);
        
        final WritableImage imageFX = new WritableImage(16,16);
        SwingFXUtils.toFXImage(bImage, imageFX);
        
        mapFX.put(iconName, imageFX);
    }
    
    public static javafx.scene.image.Image getFXAlertIcon(String iconName, int w, int h){
        IconManager currentIMObj = getIconManager();
        String key = "icons/black/" + iconName;
        Image iObj = ImageUtilities.loadImage(key);
        if(iObj.getHeight(null) != h && iObj.getWidth(null) != w){
            iObj = currentIMObj.scaleImage(iObj, w, h);
        }
        
        BufferedImageBuilder builder = new BufferedImageBuilder();
        BufferedImage bImage = builder.bufferImage(iObj);
        
        final WritableImage imageFX = new WritableImage(w,h);
        SwingFXUtils.toFXImage(bImage, imageFX);
        return (javafx.scene.image.Image) imageFX;
    }

}
