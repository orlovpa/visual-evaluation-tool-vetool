package ru.ptahi.aet.ibsvisualizer;
/** Localizable strings for {@link ru.ptahi.aet.ibsvisualizer}. */
@javax.annotation.Generated(value="org.netbeans.modules.openide.util.NbBundleProcessor")
class Bundle {
    /**
     * @return <i>Source</i>
     * @see IBSVisualizerDataObject
     */
    static String LBL_IBSVisualizer_EDITOR() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "LBL_IBSVisualizer_EDITOR");
    }
    /**
     * @return <i>Files of IBSVisualizer</i>
     * @see IBSVisualizerDataObject
     */
    static String LBL_IBSVisualizer_LOADER() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "LBL_IBSVisualizer_LOADER");
    }
    /**
     * @return <i>Visual</i>
     * @see IBSVisualizerVisualElement
     */
    static String LBL_IBSVisualizer_VISUAL() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "LBL_IBSVisualizer_VISUAL");
    }
    private void Bundle() {}
}
