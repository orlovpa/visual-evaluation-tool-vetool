package ru.ptahi.aet.ibsvisualizer;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openide.loaders.DataObject;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author paulorlov
 */
public class TobiiSourceLineParser extends ChildFactory<IntervalBetweenSaccades>{
    private DataObject dObj;
    private List<String> tobiiSourceLines;
//    private List<IntervalBetweenSaccades> ibsList = new ArrayList<IntervalBetweenSaccades>();
            
    TobiiSourceLineParser(DataObject dObj) {
        this.dObj = dObj;
    }

    public void parse(List<IntervalBetweenSaccades> list) {
//        DataObject dObj = (DataObject) nObj.getLookup().lookup(DataObject.class);
        if(dObj == null){
            return;
        }
        try {
            tobiiSourceLines = dObj.getPrimaryFile().asLines();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        for(int i = 1; i < tobiiSourceLines.size(); i++){
            String currentLine = tobiiSourceLines.get(i);
            IntervalBetweenSaccades ibsObj = new IntervalBetweenSaccades();
            String currentString = "";
            int stringIndex = 0;
            for(int j = 0; j < currentLine.length(); j++){
                char c = currentLine.charAt(j);
                if(c != '\t'){
                    currentString += c;
                } else {
                    if(currentString != ""){
                        if(stringIndex == 5){
                            ibsObj.setStartTime(Long.parseLong(currentString));
                        } else if(stringIndex == 6){
                            ibsObj.setLength(Long.parseLong(currentString));
                        }
                        stringIndex++;
                        currentString = "";
                    }
                }
                
                if(j == currentLine.length() - 1){
                    currentString = "";
                }
            }
            //ibsList.add(ibsObj);
            list.add(ibsObj);
        }
    }
    
    @Override
    protected boolean createKeys(List<IntervalBetweenSaccades> list) {
        //Runs on backround thread
        parse(list);
        return true;
    }
    
    @Override
    protected Node createNodeForKey(IntervalBetweenSaccades key) {
        IntervalBetweenSaccadesNode isbNodeObj = null;
        InstanceContent iContent = new InstanceContent();
//      iContent.add("Some...");
        try {
            isbNodeObj = new IntervalBetweenSaccadesNode(key, iContent);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return isbNodeObj;
    }
}
