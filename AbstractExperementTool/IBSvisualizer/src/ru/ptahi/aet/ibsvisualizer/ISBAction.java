/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.ptahi.aet.ibsvisualizer;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import static javax.swing.Action.NAME;
import org.openide.util.Lookup;

/**
 *
 * @author paulorlov
 */
public class ISBAction extends AbstractAction {
    
    Lookup lookup;

    ISBAction(Lookup lookup) {
        putValue(NAME, "Visualise");
        this.lookup = lookup;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }
    
}
