package ru.ptahi.aet.ibsvisualizer;

import java.beans.IntrospectionException;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author paulorlov
 */
public class IntervalBetweenSaccadesNode extends BeanNode<IntervalBetweenSaccades> {
    InstanceContent iContent;

    public IntervalBetweenSaccadesNode(IntervalBetweenSaccades bean, InstanceContent iContent) throws IntrospectionException {
        super(bean, Children.LEAF, new AbstractLookup(iContent));
        this.iContent.add(bean);
        setDisplayName(bean.getStartTime() + " " + bean.getLength());
    }
    
}
