package ru.ptahi.aet.ibsvisualizer;

/**
 *
 * @author paulorlov
 */
public class IntervalBetweenSaccades {
    private long startTime;
    private long length;
    
    public IntervalBetweenSaccades(){
        
    }

    public IntervalBetweenSaccades(long startTime, long length) {
        this.startTime = startTime;
        this.length = length;
    }
    
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }
    
}
