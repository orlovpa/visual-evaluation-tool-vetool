package ru.ptahi.aetexperiment;

import com.google.gson.Gson;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import ru.ptahi.aet.participant.Participant;

/**
 *
 * @author paulorlov
 */
class Experiment {
    private int id;
    private String comment;
    private Participant participant;
    private List listeners = Collections.synchronizedList(new LinkedList());
    private Experiment.FF_Experiment ffExperimentObj = new FF_Experiment();
    private ArrayList<Annotation> fdList;
    private ArrayList<Annotation> eaList;

    void setFixationDurationList(ArrayList<Annotation> fdList) {
        ArrayList<Annotation> oldfdList = this.fdList;
        this.fdList = fdList;
        fire("fdList", oldfdList, fdList);
    }
    
    public ArrayList<Annotation> getFdList() {
        return fdList;
    }

    void setELANAnnotationList(ArrayList<Annotation> eaList) {
        ArrayList<Annotation> oldEaList = this.eaList;
        this.eaList = eaList;
        fire("eaList", oldEaList, eaList);        
    }

    public ArrayList<Annotation> getEaList() {
        return eaList;
    }
    
    public static class FF_Experiment {
        public int id;
        public String comment;
        public String participant;
        
        public String _ff_id;
        public int _ff_author_id;

    }
    
    public FF_Experiment getFfExperimentObj() {
        return ffExperimentObj;
    }

    public void setFfParticipantObj(FF_Experiment ffParticipantObj) {
        this.ffExperimentObj = ffParticipantObj;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        listeners.add(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        listeners.remove(pcl);
    }

    private void fire(String propertyName, Object old, Object nue) {
        //Passing 0 below on purpose, so you only synchronize for one atomic call:
        PropertyChangeListener[] pcls = (PropertyChangeListener[]) listeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < pcls.length; i++) {
            pcls[i].propertyChange(new PropertyChangeEvent(this, propertyName, old, nue));
        }
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldComment = this.comment;
        this.comment = comment;
        fire("comment", oldComment, comment);
    }
    
    public String getJSON(){
        String result = null;
        Gson gson = new Gson();
        ffExperimentObj.id = id;
        ffExperimentObj.comment = comment;
        ffExperimentObj.participant = participant.getJSON();
        result = gson.toJson(ffExperimentObj);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        int oldId = this.id;
        this.id = id;
        fire("id", oldId, id);
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        Participant oldParticipant = this.participant;
        this.participant = participant;
        fire("participant", oldParticipant, participant);
    }
    
    
}
