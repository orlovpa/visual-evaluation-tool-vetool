package ru.ptahi.aetexperiment;
/** Localizable strings for {@link ru.ptahi.aetexperiment}. */
@javax.annotation.Generated(value="org.netbeans.modules.openide.util.NbBundleProcessor")
class Bundle {
    /**
     * @return <i>ExperimentVisualizer</i>
     * @see ExperimentVisualizerTopComponent
     */
    static String CTL_ExperimentVisualizerAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_ExperimentVisualizerAction");
    }
    /**
     * @return <i>ExperimentVisualizer Window</i>
     * @see ExperimentVisualizerTopComponent
     */
    static String CTL_ExperimentVisualizerTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_ExperimentVisualizerTopComponent");
    }
    /**
     * @return <i>Visualize Experiment</i>
     * @see Visualize
     */
    static String CTL_Visualize() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_Visualize");
    }
    /**
     * @return <i>VisualizerProperties</i>
     * @see VisualizerPropertiesTopComponent
     */
    static String CTL_VisualizerPropertiesAction() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_VisualizerPropertiesAction");
    }
    /**
     * @return <i>VisualizerProperties Window</i>
     * @see VisualizerPropertiesTopComponent
     */
    static String CTL_VisualizerPropertiesTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "CTL_VisualizerPropertiesTopComponent");
    }
    /**
     * @return <i>This is a ExperimentVisualizer window</i>
     * @see ExperimentVisualizerTopComponent
     */
    static String HINT_ExperimentVisualizerTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_ExperimentVisualizerTopComponent");
    }
    /**
     * @return <i>This is a VisualizerProperties window</i>
     * @see VisualizerPropertiesTopComponent
     */
    static String HINT_VisualizerPropertiesTopComponent() {
        return org.openide.util.NbBundle.getMessage(Bundle.class, "HINT_VisualizerPropertiesTopComponent");
    }
    private void Bundle() {}
}
